import NavigationBar from '../components/global/NavigationBar.js';

const HomePage = () => {
    return (
        <>
            <NavigationBar />
        </>
    );
}

export default HomePage;