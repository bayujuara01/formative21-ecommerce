import { Card, Container, Button, Row, Col, Form } from "react-bootstrap";
import { useContext } from 'react';
import { NavigationContext } from '../../contexts/NavigationContext.js';

const AccountCard = () => {
    const [login, setLogin] = useContext(NavigationContext);

    const clickHandler = () => {
        setLogin(!login);
    }

    return (
        <Card className="mx-auto user-card" style={{ width: '24rem', borderRadius: '8px' }}>

            <Card.Body>
                <Card.Title className="mt-4 mb-4">Masuk</Card.Title>
                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label className="text-muted user-input__label">Alamat Email</Form.Label>
                        <Form.Control type="email" placeholder="Masukkan email" />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label className="text-muted user-input__label">Kata Sandi</Form.Label>
                        <Form.Control type="password" placeholder="Masukkan Password" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox" >
                        <p className="user-link__right user-input__label">
                            <a href="#home" className="text-success user-link " >Lupa Kata Sandi?</a>
                        </p>
                    </Form.Group>

                </Form>
                <Row className="mb-4">
                    <Col>
                        <Button style={{ width: '100%', borderRadius: '8px' }} variant="success" onClick={clickHandler}>Masuk</Button>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col className="d-flex justify-content-center align-items-center">
                        <span className="user-account-line"></span>
                        <span className="user-account-line__label text-muted mx-3">Atau masuk dengan</span>
                        <span className="user-account-line"></span>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col>
                        <Button style={{ width: '100%', borderRadius: '8px' }} variant="outline-secondary">Google</Button>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col>
                        <Button style={{ width: '100%', borderRadius: '8px' }} variant="outline-secondary">Facebook</Button>
                    </Col>
                </Row>
            </Card.Body>

        </Card>
    );
}

export default AccountCard;