import { useContext } from 'react';
import { NavigationContext } from '../../contexts/NavigationContext.js';
import { Nav, Navbar, Container, Form, FormControl, Button } from 'react-bootstrap';
import logo from '../../assets/images/tokopaedi-logo.png';

const NavigationBar = () => {
    const [login, setLogin] = useContext(NavigationContext);

    const clickHandler = () => {
        setLogin(!login);
    }

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="#home">
                    <img
                        src={logo}

                        height="40"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className=" my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        <Nav.Link href="#action2">Kategori</Nav.Link>
                    </Nav>
                    <Form className="d-flex w-100" >
                        <FormControl
                            type="search"
                            placeholder="Cari Barang"
                            className="me-2"
                            aria-label="Cari Barang"
                            style={{ width: '100%' }}
                        />
                        <Button variant="outline-success" className="mx-2">Cari</Button>
                        <Button onClick={clickHandler} variant="outline-success">Keluar</Button>
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;