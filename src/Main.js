
import LoginPage from './screens/LoginPage';
import HomePage from './screens/HomePage';
import { NavigationProvider, NavigationContext } from './contexts/NavigationContext';
import { useContext } from 'react';

const Main = () => {
    const [login, setLogin] = useContext(NavigationContext);

    return (
        <>
            {login ? <HomePage /> : <LoginPage />}
        </>
    );
}

export default Main;
