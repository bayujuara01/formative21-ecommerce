import React, { useState, createContext } from 'react';


const NavigationContext = createContext();

const NavigationProvider = (props) => {
    const [login, setLogin] = useState(false);
    return (
        <div>
            <NavigationContext.Provider value={[login, setLogin]}>
                {props.children}
            </NavigationContext.Provider>
        </div>
    );
}

export { NavigationContext, NavigationProvider };