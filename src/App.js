import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavigationProvider } from './contexts/NavigationContext';
import Main from './Main.js';

function App() {


  return (
    <NavigationProvider>
      <Main />
    </NavigationProvider>
  );
}

export default App;
